import RPi.GPIO as GPIO
import time
 
global TOP 

TOP = True
# Define GPIO to LCD mapping
LCD_RS = 7
LCD_E  = 8
LCD_E_bottom = 21
LCD_D4 = 25
LCD_D5 = 24
LCD_D6 = 23
LCD_D7 = 18
 
bt_v = 0.0
pv_v = 33.33
pv_i = 1.9

 
# Define some device constants
LCD_WIDTH = 40    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False
 
LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
#LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line ??
#LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line ??


# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005
 
def main():
  # Main program block
  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
  GPIO.setup(LCD_E, GPIO.OUT)  # E
  GPIO.setup(LCD_E_bottom, GPIO.OUT)
  GPIO.setup(LCD_RS, GPIO.OUT) # RS
  GPIO.setup(LCD_D4, GPIO.OUT) # DB4
  GPIO.setup(LCD_D5, GPIO.OUT) # DB5
  GPIO.setup(LCD_D6, GPIO.OUT) # DB6
  GPIO.setup(LCD_D7, GPIO.OUT) # DB7
 
  # Initialise display
  lcd_init()
 
  while True:
    #Send some test
    lcd_string("PanelVoltage: " + str(pv_v) + " PanelCurrent: " + str(pv_i),  LCD_LINE_1, TOP = True)
    lcd_string("BatteryVoltage: " + str(bt_v),LCD_LINE_2, TOP = True)
    #lcd_string("Does this work", LCD_LINE_1, TOP = False)
 
    time.sleep(3) # 3 second delay
 
    # Send some text
    lcd_string("1234567890123456",LCD_LINE_1, TOP = True)
    lcd_string("abcdefghijklmnop",LCD_LINE_2, TOP = True)
 
    time.sleep(3) # 3 second delay
	
    lcd_string("TESTING 1234", LCD_LINE_1, TOP=False)
    lcd_string("TESTING 1234", LCD_LINE_2, TOP=False)
	
    time.sleep(3)
	
 

 
def lcd_init():
  # Initialise display
  Top = True
  lcd_byte(0x33,LCD_CMD, TOP) # 110011 Initialise
  lcd_byte(0x32,LCD_CMD, TOP) # 110010 Initialise
  lcd_byte(0x06,LCD_CMD, TOP) # 000110 Cursor move direction
  lcd_byte(0x0C,LCD_CMD, TOP) # 001100 Display On,Cursor Off, Blink Off
  lcd_byte(0x28,LCD_CMD, TOP) # 101000 Data length, number of lines, font size
  lcd_byte(0x01,LCD_CMD, TOP) # 000001 Clear display
  time.sleep(E_DELAY)
  
  TOP = False
  lcd_byte(0x33,LCD_CMD, TOP) # 110011 Initialise
  lcd_byte(0x32,LCD_CMD, TOP) # 110010 Initialise
  lcd_byte(0x06,LCD_CMD, TOP) # 000110 Cursor move direction
  lcd_byte(0x0C,LCD_CMD, TOP) # 001100 Display On,Cursor Off, Blink Off
  lcd_byte(0x28,LCD_CMD, TOP) # 101000 Data length, number of lines, font size
  lcd_byte(0x01,LCD_CMD, TOP) # 000001 Clear display
  time.sleep(E_DELAY)
 
def lcd_byte(bits, mode, TOP):
  # Send byte to data pins
  # bits = data
  # mode = True  for character
  #        False for command
 
  #BOTTOM = !TOP
  GPIO.output(LCD_RS, mode) # RS
 
  # High bits
  GPIO.output(LCD_D4, False)
  GPIO.output(LCD_D5, False)
  GPIO.output(LCD_D6, False)
  GPIO.output(LCD_D7, False)
  if bits&0x10==0x10:
    GPIO.output(LCD_D4, True)
  if bits&0x20==0x20:
    GPIO.output(LCD_D5, True)
  if bits&0x40==0x40:
    GPIO.output(LCD_D6, True)
  if bits&0x80==0x80:
    GPIO.output(LCD_D7, True)
 
  if (TOP):
  # Toggle 'Enable' pin
    lcd_toggle_enable()
  else:
    lcd_toggle_enable_bottom()
 
 
  # Low bits
  GPIO.output(LCD_D4, False)
  GPIO.output(LCD_D5, False)
  GPIO.output(LCD_D6, False)
  GPIO.output(LCD_D7, False)
  if bits&0x01==0x01:
    GPIO.output(LCD_D4, True)
  if bits&0x02==0x02:
    GPIO.output(LCD_D5, True)
  if bits&0x04==0x04:
    GPIO.output(LCD_D6, True)
  if bits&0x08==0x08:
    GPIO.output(LCD_D7, True)
 
  # Toggle 'Enable' pin
  if (TOP):
  # Toggle 'Enable' pin
    lcd_toggle_enable()
  else:
    lcd_toggle_enable_bottom()
 
def lcd_toggle_enable():
  # Toggle enable
  time.sleep(E_DELAY)
  GPIO.output(LCD_E, True)
  time.sleep(E_PULSE)
  GPIO.output(LCD_E, False)
  time.sleep(E_DELAY)
  
def lcd_toggle_enable_bottom():
  # Toggle enable
  time.sleep(E_DELAY)
  GPIO.output(LCD_E_bottom, True)
  time.sleep(E_PULSE)
  GPIO.output(LCD_E_bottom, False)
  time.sleep(E_DELAY)
 
def lcd_string(message,line,TOP):
  # Send string to display
 
  message = message.ljust(LCD_WIDTH," ")
 
  lcd_byte(line, LCD_CMD, TOP)
 
  for i in range(LCD_WIDTH):
    lcd_byte(ord(message[i]),LCD_CHR, TOP)
 
if __name__ == '__main__':
 
  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    Top=True
    lcd_byte(0x01, LCD_CMD, TOP)
    lcd_string("See ya!",LCD_LINE_1, TOP)
    GPIO.cleanup()
